### 0.2.1 - 2015-08-25
* Eliminated the unnecessary dependency to MetaOCaml

### 0.2.0 - 2015-03-22
* Measured the example query's performance (see `example1.ml` and `example_tlinq.ml`)
* Added normal form checking in the recursive module
* Implemented all of normalization rules
* Added tesing code for the normalization
* Removed label and record types that are unnecessary
* Complement some incomplete code

### 0.1.0 - 2015-03-03
* Initial release
